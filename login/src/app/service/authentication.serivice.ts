import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user.model';




@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public user:User
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
       
       this.authenticate(username,password).subscribe(currentUseer=>this.user=currentUseer)
       
     
                // login successful if there's a jwt token in the response
                if (this.user && this.user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(this.user));
                    this.currentUserSubject.next(this.user);
                }

                return this.user;
            
    }
    private authenticate(username: string, password: string) {
       
        if(username==='demo' && password==='password1')        
        return of({
            id: "1234",
            username: "demo",
            firstName: 'demo user',
            lastName: 'last',
            token: '32323232324342342344'
        })
        else
        {
            return of(null);
        }
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}