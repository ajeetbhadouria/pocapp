import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './service/authentication.serivice';
import { Router } from '@angular/router';
import { User } from './models/user.model';
import * as singleSpa from 'single-spa';
import * as appdata from "./json/appconfigure.json";

declare var SystemJS: any;
declare var document: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
 
export class AppComponent implements OnInit  {
  currentUser: User;
  navigateTo = url => window.history.pushState(null, null, url)
  ngOnInit() {
    const mountableApps =    appdata.metaData;
   const loadingPromises = [];
   
   for (let [key, app] of Object.entries(mountableApps)){
         this.loadApp(app.Name, app.Hash, app.AppUrl,app.default);
  }
  
  singleSpa.start();
 }
  constructor(
      private router: Router,
      private authenticationService: AuthenticationService
  ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
  }
  loadApp(name, hash, appURL,isloaded) {
    // let storeModule = {}, customProps = {globalEventDistributor: globalEventDistributor};
       
     // try to import the store module
     // try {
     //   //  storeModule = storeURL ?  SystemJS.import(storeURL) : {storeInstance: null};
     // } catch (e) {
     //     console.log(`Could not load store of app ${name}.`, e);
     // }
 
     // if (storeModule.storeInstance && globalEventDistributor) {
     //     // add a reference of the store to the customProps
     //     customProps.store = storeModule.storeInstance;
 
     //     // register the store with the globalEventDistributor
     //     globalEventDistributor.registerStore(storeModule.storeInstance);
     // }
    
    // var res=SystemJS.import(appURL)
    const status=singleSpa.getAppStatus(name)
    if(!status)
    {
    // register the app with singleSPA and pass a reference to the store of the app as well as a reference to the globalEventDistributor
    singleSpa.registerApplication(name, () => SystemJS.import(appURL), (location) => isloaded==true|| 
    location.pathname.startsWith( `/${name}`));
   }
    }
}